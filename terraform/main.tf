terraform {
  required_version = ">= 1.6.1"
  backend "s3" {
    bucket = "twwn-capstore-proj"
    key    = "state.tfstate"
    region = "ap-southeast-1"
  }
}
