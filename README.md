# Introduction

This project is a sample implementation of all the knowledge learned in the DevOps Bootcamp from Techworld with Nana for Module 15.

The objectives here are similar to what is being asked on the Capstone Project 2.

NOTE: This Capstone Project deploys the same application (spring-petclinic) as Capstone Project 1 (https://gitlab.com/twwnana/capstoneprojects/capstone-proj01).

<img width="650" alt="petclinic-screenshot" src="https://lh3.googleusercontent.com/DURuHF7ZZ3s32ULAjQM1FHHH-NL8DJSVG6RUBzyFusGyu4lrCJ9w5IVm80uhQ4L4ps6xW591rK6QHO74nz-dR4yNj4cGISF8MC-HD0IVHarJYJOp7TtOTRYCto80IZOwPA=w315">

Bootcamp Chapter 15 Git Repo: 
- git@gitlab.com:twwnana/ansible/chapter15-lecture.git
- git@gitlab.com:twwnana/ansible/jenkins-integration.git

# Reqiurements before running the Jenkins Pipeline

- ECR Container must exist (You must manually create this ECR Repo on your tenant account.).
- AWS Bucket for Terraform State must exist (You must manually created on your tenant account.).
- EKS Cluster must exist (This can be created using terraform which is included in this repository.).
- AWS cli tool must already been installed and the command "aws configure" set up.

# Creating the EKS Cluster (must be done before running the pipeline)

1. Create the AWS Bucket for the terraform state
2. Clone the resitory git@gitlab.com:twwnana/capstoneprojects/capstone-proj01.git
3. Update the main.tf to refer to the bucket name you created and the default region.
3. From the project root create the file terraform\custom-variables\vpc-variables-tfvars and update the following fields appropriately.
```
app_name       = "petclinic"
cluster_name   = "petclinic_k8s_cluster"
env            = "dev"
instance_types = "t2.medium"
vpc_cidr_block = "10.0.0.0/16"
private_subnets_cidr_blocks = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24"]
public_subnets_cidr_blocks  = ["10.0.3.0/24", "10.0.4.0/24", "10.0.5.0/24"]

```
4. Navigate to the Terraform folder and issue the command below to intialize terraform.
```
terraform init
```
5. Apply the terraform configuration to create the EKS cluster. NOTE: This will take a while to complete.
```
terraform apply -var-file custom-variables/vpc-variables.tfvars -auto-approve
```

# Required variables

There are environment variables that needs to exist before we can run the playbook, these variables will contain the information that the play needs to create the resources properly.
- MYSQL_USER - This will be the sql user that will be deployed in the database and is the account used by petclinic to access and initialize database contents.
- MYSQL_PASS - The password for MYSQL_USER.
- IMAGE_NAME - This will be the ECR image name.
- ECR_REPO_NAME_PASSWORD - The Password to use to access teh ECR image.

# Required libraries

The collection library "commuinity.kubernetes" should be installed to the endpoint executing the playbook. This can be done by the following command (assuming ansible-galaxy is installed)
```
ansible-galaxy collection install community.kubernetes
```

# Running the Ansible Play
Do the following steps where ansible is installed.

1. Clone the library git@gitlab.com:twwnana/capstoneprojects/capstone-proj02.git
2. Generate the ECR password. (Issue the command below, aws configure must already be set up).
```
aws ecr get-login-password --region ap-southeast-1
```
3. Run the Playbook
```
ansible-playbook deploy-to-k8s.yaml -e "MYSQL_USER=<desired_username_for_mysqldb> MYSQL_PASS=<desired_password_for_mysqldb> IMAGE_NAME=<IMAGE_NAME>:<IMAGE_TAG> ECR_REPO_NAME_PASSWORD=<ECR_PASSWORD>
```
Sample:
```
ansible-playbook deploy-to-k8s.yaml -e "MYSQL_USER=petclinic MYSQL_PASS=petclinic IMAGE_NAME=951865328368.dkr.ecr.ap-southeast-1.amazonaws.com/petclinic:3.10.0-32 ECR_REPO_NAME_PASSWORD=eyJwYXlsb2FkIjoiTWdnUW5ubU51b0JKSWlsKzMxWjVDb1BDbnVweDJ3MmtveXBsNXprVzg1Z3lkRXUxRC9PbzNFSWdCVEdFQVdUSytGaXp6dXUzT0FtWTZxbFNCOFJjYUpWcW4vUVMvbUF3TkFWL0tNOUpQN25SbGJVektueVFiNWZMVjJQaWVkNmFJbG81WFliY2Y3ZG15Z1B3RCt0TGwvakFEMXN0Z2JoVU4weFNxWkNYYzJLS2dGaUtnTlkrL0dlOHM1TmpCU1UxSE1wVlhqN2J4WFdIY1dZVFE1UVphcUtVMzVZMTBxQWl2Y201cUErQ0xPTTd2THh4SUM4dy84WTZkQmQ2anRYRDZSU0Q2czNqazJTWGcwbnphSis5UTBtNFVFYVlUbFpadjcrd0htTFJLLzhxa3ZOOXk5YmJ2WUJ0TGFxT21QNDZqdVphcjRod0VuT2pFUUQ5eHNyZXN5TXNWcFYzbFdmL21QV3cvTjFxNXhvYVBwWlNvUjNyVkp3aXlOZkVNYk5qcWE5SGZzMzluMkJMOHBIUndyVGRIK1NWSDZPcTMyek83Wkh2Z01iYjQwVy84NXBmb0dnbFVHb2lSTlNtc0tSdDlYWkxVZipf825nV2NmU3lPeDVGdGkvckxseXN5Vno1Q0kwRW9idDNMT2ExVExSbXBQVERRd3VNZTRQMHZFRWhkam4xOGtNUHd2RXN6R2d2ZXpZOTg4WllkejJFaGt1bC9VMTJDYldHcWFJcmZEdUFmT09DaG1CZzFRYzZvVTU1M3j8TVIrQ1pxaVg4cjdsWW1NLzRhN29yMUhMd2MwMXdGeGRVczE4VktrSkVGMkxmdUNZRSs1QVhuSkhsOUhLcFVVaFYzYlM2S0pVYmxrdW5nSTBwc2JxeTl1R2tmL2lkbGZyOW5EVUtUY1I2aGRDVHBWUEdQRGxlK2lTOHBVNEFwZFZzRnNmWFRiMlJEazNnMVNYSE9vS1RqM0pVRElkWlpTd1NBR2V1OXplS1RqZjQ3eVgyNk54MWVXc1NVMklMcnkwaFcyQkQ0cWYzUzlWRDFSUGVDK1FKbGUxVjRNbC9aYU5mQVo2TjFEcjJCNFhMelcxTTMzSThVcFBUTDBzaVJBREp0MWFnVUhvMjZ5bHZYNWZXa3cwQnM1RXVIWXM0cG1pbGhZOW5GTzgxenFGZ09iRmY4Tm9yb0V4Y3Zoc3Vhc0wvRmxDSEdPdXU2ZUZscU82ZytHTVo0dXhvaGJ6aWdVNUdJRUVUT0JaaXN0QlcvNjFGWUFIQzNIeFFDSjMxNDlPMjB4YmNySnNWbDhyQVFUbGRNWEhvUEsvNTNBemNXNFU0RnUrd0wrbVRyV0dxMFJ4VVlkZGJUTTlNdmxySWdjZHQ2K3lZM082VllTWWVaU2VKaUROdWx3QWVGSXlmWWVJMzJBWT0iLCJkYXRha2V5IjoiQVFFQkFIaWRFclpDZmhLT2VETTA4K2NQNWZ0eWp2UTlYTU1TUTRwSzBGWm52QVpYSmdBQUFINHdmQVlKS29aSWh2Y05BUWNHb0c4d2JRSUJBREJvQmdrcWhraUc5dzBCQndFd0hnWUpZSVpJQVdVREJBRXVNQkVFRER3QUJnMTNUd3BuQmJqd1pnSUJFSUE3blpoTXVhTUVvUEp2TU9adlE3U3lVZlFoQ3ZrMitOTmFZVWQ3dVJYaDlPMW9qNWxXN2Rqcms0cXppNXk4VE5TU1FiR2U5STdRWC9ubmNkRT0iLCJ2ZXJzaW9uIjoiMiIsInR5cGUiOiJEQVRBX0tFWSIsImV4cGlyYXRpb24iOjE3MDI2NTMxMzZ9
```

# Testing the application Output
When the play is successful executed you can connect to the K8s cluster by executing the command below to update your kubeconfig for the K8s cluster.

NOTE: the cluster_name is dependent to the terraform variable cluster_name above
```
aws eks update-kubeconfig --name petclinic_k8s_cluster
```

Enumerate the cluster services in the default namespace and look for the "petclinic-service"
```
kubectl get service
```

Copy the EXTERNAL-IP of the petclinic-service and paste it on the browser.
NOTE: The application may take 1-2 minutes before it can service requestes so as long as you see the pods running, wait a few mintues.

## Tech Stack
**Infrastructure:** AWS ECR, AWS EKS

**Technologies:** Containerization (Docker and AWS ECR), Git (Code Versioning), Maven (application), Terraform (AWS EKS provisioning), Ansible

# Credits
The original spring-petclinic can be found at https://github.com/spring-projects/spring-petclinic